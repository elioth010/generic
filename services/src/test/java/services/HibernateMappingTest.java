/**
 * 
 */
package services;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.elioth010.generic.bs.dao.exception.ORMException;
import com.elioth010.generic.bs.service.hibernate.AbstractServiceHibernate;
import com.elioth010.generic.eis.dto.UserDTO;

/**
 * @author elioth010 @date Aug 29, 2015 8:21:17 PM
 * 
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/com/elioth010/services/bs/spring/applicationContext-*.xml", "classpath*:/com/elioth010/services/bs/spring/datasourcetest.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class HibernateMappingTest {

	@Autowired
	private AbstractServiceHibernate hibernateService;

	/**
	 * 
	 */
	public HibernateMappingTest() {
		super();
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testSelectBOs() {
		try {
			List<UserDTO> usuarios = (List<UserDTO>) getHibernateService().findAll(UserDTO.class);
			for (UserDTO userDTO : usuarios) {
				System.out.println(userDTO);
			}
		} catch (ORMException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testCreateBOs() {
		try {
			UserDTO userDTO = new UserDTO("elioth010", "d033e22ae348aeb5660fc2140aec35850c4da997", "elioth010@gmail.com", true, false, false, false, Short.parseShort("0"), new Date(), new Date(), new Date(), new Date(), "0.0.0.0", "admin", "Alfredy Elioth", "Emanuel", "Rivera", "Duarte", new Date(), false, false);
			getHibernateService().save(userDTO);
		} catch (ORMException e) {
			e.printStackTrace();
		}
	}

	public AbstractServiceHibernate getHibernateService() {
		return hibernateService;
	}

	public void setHibernateService(AbstractServiceHibernate hibernateService) {
		this.hibernateService = hibernateService;
	}

}
