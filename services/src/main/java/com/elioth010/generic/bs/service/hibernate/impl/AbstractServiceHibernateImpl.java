/**
 * 
 */
package com.elioth010.generic.bs.service.hibernate.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.elioth010.generic.bs.dao.AbstractDaoHibernate;
import com.elioth010.generic.bs.dao.exception.ORMException;
import com.elioth010.generic.bs.reflection.AbstractReflectionController;
import com.elioth010.generic.bs.service.hibernate.AbstractServiceHibernate;
import com.elioth010.generic.eis.bo.base.BaseBO;
import com.elioth010.generic.eis.dto.base.BaseDTO;
import com.elioth010.generic.util.persistence.ListaParametrosDTO;

/**
 * @author developer
 * 
 */

@Component
@Service
public class AbstractServiceHibernateImpl extends AbstractReflectionController implements AbstractServiceHibernate {
	
	@Autowired
	private AbstractDaoHibernate abstractDaoHibernate;

	@Override
	public <T> BaseDTO<?> save(BaseDTO<?> baseDTO) throws ORMException {
		BaseBO bo = this.getAbstractDaoHibernate().save(super.reflectDTO(baseDTO));
		return super.reflectBO(bo);
	}

	@Override
	public void update(BaseDTO<?> baseDTO) throws ORMException {
		this.getAbstractDaoHibernate().update(super.reflectDTO(baseDTO));
	}

	@Override
	public void delete(BaseDTO<?> baseDTO) throws ORMException{
		this.getAbstractDaoHibernate().delete(super.reflectDTO(baseDTO));
	}

	@Override
	public <T> List<? extends BaseDTO<?>> find(BaseDTO<?> baseDTO) throws ORMException{
		List<BaseDTO<?>> list = new ArrayList<>();
		List<BaseBO> lista = (List<BaseBO>) this.getAbstractDaoHibernate().find(super.reflectDTO(baseDTO));
		for (BaseBO baseBO : lista) {
			list.add(super.reflectBO(baseBO));
		}
		return list;
	}

	@Override
	public <T> BaseDTO<?> findById(Class<? extends BaseDTO<?>> clazz, Integer id) throws ORMException{
		BaseBO bo = this.getAbstractDaoHibernate().findById(super.getBOClass(clazz), id);
		return super.reflectBO(bo);
	}

	@Override
	public <T> List<? extends BaseDTO<?>> findAll(Class<? extends BaseDTO<?>> clazz) throws ORMException{
		List<BaseDTO<?>> list = new ArrayList<>();
		List<BaseBO> lista = (List<BaseBO>) this.getAbstractDaoHibernate().findAll(super.getBOClass(clazz));
		for (BaseBO baseBO : lista) {
			list.add(super.reflectBO(baseBO));
		}
		return list;
	}

	@Override
	public <T> BaseDTO<?> findWithGet(Class<? extends BaseDTO<?>> clazz, Integer id) throws ORMException{
		BaseBO bo = this.getAbstractDaoHibernate().findWithGet(super.getBOClass(clazz), id);
		return super.reflectBO(bo);
	}

	@Override
	public <T> BaseDTO<?> findWithLoad(Class<? extends BaseDTO<?>> clazz, Integer id) throws ORMException{
		BaseBO bo = this.getAbstractDaoHibernate().findWithLoad(super.getBOClass(clazz), id);
		return super.reflectBO(bo);
	}

	@Override
	public <T> List<? extends BaseDTO<?>> findByQuery(String query, Class<? extends BaseDTO<?>> clazz, ListaParametrosDTO parametros) throws ORMException{
		List<BaseDTO<?>> list = new ArrayList<>();
		List<BaseBO> lista = (List<BaseBO>) this.getAbstractDaoHibernate().findByQuery(query,super.getBOClass(clazz),parametros);
		for (BaseBO baseBO : lista) {
			list.add(super.reflectBO(baseBO));
		}
		return list;
	}

	@Override
	public <T> List<? extends BaseDTO<?>> findBySQLQuery(String query, Class<? extends BaseDTO<?>> clazz, ListaParametrosDTO parametros) throws ORMException{
		List<BaseDTO<?>> list = new ArrayList<>();
 		List<BaseBO> lista = (List<BaseBO>) this.getAbstractDaoHibernate().findBySQLQuery(query,super.getBOClass(clazz),parametros);
		for (BaseBO baseBO : lista) {
			list.add(super.reflectBO(baseBO));
		}
		return list;
	}

	public AbstractDaoHibernate getAbstractDaoHibernate() {
		return abstractDaoHibernate;
	}

	public void setAbstractDaoHibernate(AbstractDaoHibernate abstractDaoHibernate) {
		this.abstractDaoHibernate = abstractDaoHibernate;
	}

}