/**
 * 
 */
package com.elioth010.generic.bs.interceptor.util;

import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.elioth010.generic.eis.persistence.audit.AuditLog;
import com.elioth010.generic.eis.persistence.audit.AuditLogRecord;

/**
 * @author elioth010
 *
 */
public class AuditLogUtil {
	
	private static final Logger logger = Logger.getLogger(AuditLogUtil.class);
	
	public static void LogIt(String action, AuditLog entity, Session session, String userUpdate, String ip) {
		try {
			AuditLogRecord auditRecord = new AuditLogRecord(action, entity.getLogDeatil(), new Date(), entity.getId().toString(), entity.getClass().toString(), ip, userUpdate);
			session.save(auditRecord);
		} catch(Exception e) {
			logger.error(e);
		}
	}
}
