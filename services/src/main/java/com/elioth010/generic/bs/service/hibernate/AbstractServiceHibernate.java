package com.elioth010.generic.bs.service.hibernate;

import java.util.List;

import com.elioth010.generic.bs.dao.exception.ORMException;
import com.elioth010.generic.eis.dto.base.BaseDTO;
import com.elioth010.generic.util.persistence.ListaParametrosDTO;

public interface AbstractServiceHibernate {
	
	public <T> BaseDTO<?> save(BaseDTO<?> baseDTO) throws ORMException;

	public void update(BaseDTO<?> baseDTO) throws ORMException;

	public void delete(BaseDTO<?> baseDTO) throws ORMException;

	public <T> List<? extends BaseDTO<?>> find(BaseDTO<?> baseDTO) throws ORMException;

	public <T>BaseDTO<?> findById(Class<? extends BaseDTO<?>> clazz, Integer id) throws ORMException;

	public <T> List<? extends BaseDTO<?>> findAll(Class<? extends BaseDTO<?>> clazz) throws ORMException;
	
	public <T>BaseDTO<?> findWithGet(Class<? extends BaseDTO<?>> clazz, Integer id) throws ORMException;
	
	public <T>BaseDTO<?> findWithLoad(Class<? extends BaseDTO<?>> clazz, Integer id) throws ORMException;
	
	public <T> List<? extends BaseDTO<?>> findByQuery(String query, Class<? extends BaseDTO<?>> clazz, ListaParametrosDTO parametros) throws ORMException;

	public <T> List<? extends BaseDTO<?>> findBySQLQuery(String query, Class<? extends BaseDTO<?>> clazz, ListaParametrosDTO parametros) throws ORMException;
}
