package com.elioth010.generic.bs.service.hibernate;

import java.util.List;

import com.elioth010.generic.eis.dto.UserDTO;

public interface UserService {
	public UserDTO findUserByUsername(String username);
	
	public List<UserDTO> findAllUsers();
}
