/**
 * 
 */
package com.elioth010.generic.bs.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.elioth010.generic.eis.bo.base.BaseBO;
import com.elioth010.generic.eis.dto.base.BaseDTO;

/**
 * @author elioth010 created: 4/12/2014 
 * Using Eclipse IDE with Spring Framework
 *
 * Se encarga de manejar la reflexión de objetos en java
 * 
 */
public abstract class AbstractReflectionController {
	
	private static final Logger logger = Logger.getLogger(AbstractReflectionController.class);
	
	public AbstractReflectionController() {

	}
	
	/**
	 * 
	 * @param dto
	 * @return la refleccion de los atributos del DTO en el BO
	 */
	@SuppressWarnings("unchecked")
	public BaseBO reflectDTO (BaseDTO<?> dto){
		try {
			//Generamos la instancia del BO a partir del DTO
			BaseBO bo = (BaseBO) getBOClass(dto).newInstance();
			for (Field dtoField : dto.getClass().getDeclaredFields()) {
				dtoField.setAccessible(true);
				Field boField = bo.getClass().getDeclaredField(dtoField.getName());
				if(boField.getName().contains("serialVersionUID")){
					//throw new IllegalArgumentException();
					continue;
				}if(dtoField.getType().getSuperclass()!=null && dtoField.getType().getSuperclass().equals(BaseDTO.class)){
					boField.setAccessible(true);
					if(dtoField.get(dto)!=null){
						boField.set(bo, reflectDTO((BaseDTO<?>) dtoField.get(dto)));
					}else{
						boField.set(bo, null);
					}
					continue;
				}else{
					boField.setAccessible(true);
					if(dtoField.getType().isInstance(new HashSet<BaseDTO<?>>())){
						Set<BaseBO> set = new HashSet<BaseBO>();
						if(dtoField.get(dto)!=null){
							for(BaseDTO<?> baseDTO : (Set<BaseDTO<?>>)dtoField.get(dto)){
								//BaseDTO<?> setDTO = (BaseDTO<?>) Class.forName(basebBO.getClass().getName().replace(".bo.", ".dto.")+"DTO").newInstance();
								set.add(reflectDTO(baseDTO));
							}
						}
						boField.set(bo, set);
					}else{
						boField.set(bo, dtoField.get(dto));
					}
				}
			}
			
			//Copiamos las propiedades
			//BeanUtils.copyProperties(bo, dto);
			logger.info(bo);
			//Retornamos el objeto transformado
			return bo;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public BaseDTO<?> reflectBO(BaseBO bo){
		try {
			//Generamos la instancia del BO a partir del DTO
			BaseDTO<?> dto = (BaseDTO<?>) Class.forName(bo.getClass().getName().replace(".bo.", ".dto.")+"DTO").newInstance();
 			
			for (Field boField : bo.getClass().getDeclaredFields()) {
				boField.setAccessible(true);
				Field dtoField = dto.getClass().getDeclaredField(boField.getName());
				if(boField.getName().contains("serialVersionUID")){
					//throw new IllegalArgumentException();
					continue;
				}else if(boField.getType().getSuperclass()!=null && boField.getType().getSuperclass().equals(BaseBO.class)){
					dtoField.setAccessible(true);
					if(boField.get(bo)!=null){
						dtoField.set(dto, reflectBO((BaseBO) boField.get(bo)));
					}else{
						dtoField.set(dto, null);
					}
					continue;
				}else{
					dtoField.setAccessible(true);
					if(boField.getType().isInstance(new HashSet<BaseBO>())){
						Set<BaseDTO<?>> set = new HashSet<BaseDTO<?>>();
						if(boField.get(bo)!=null){
							for(BaseBO baseBO : (Set<BaseBO>)boField.get(bo)){
								//BaseDTO<?> setDTO = (BaseDTO<?>) Class.forName(basebBO.getClass().getName().replace(".bo.", ".dto.")+"DTO").newInstance();
								set.add(reflectBO(baseBO));
							}
						}
						dtoField.set(dto, set);
					}else{
						dtoField.set(dto, boField.get(bo));
					}
				}
			}
			//Copiamos las propiedades
			//BeanUtils.copyProperties(bo, dto);
			logger.info(dto);
			//Retornamos el objeto transformado
			return dto;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 
	 * @param dto
	 * @return retorna la Clase que recibio de argumento el DTO
	 * para recuperar la instancia y hacer la reflexion
	 *  
	 */
	@SuppressWarnings("unchecked")
	public Class<? extends BaseBO> getBOClass(BaseDTO<?> dto){
		Class<? extends BaseBO> type = null;
		//Pide el tipo primitivo de mi superclase
		ParameterizedType typeParameterized = (ParameterizedType) dto.getClass().getGenericSuperclass();
		//Toma el tipo class de mi argumento que tiene BaseDTO
		type = (Class<? extends BaseBO>) typeParameterized.getActualTypeArguments()[0];
		//Retorna el tipo class
		return type;
	}
	
	@SuppressWarnings("unchecked")
	public Class<? extends BaseBO> getBOClass(Class<? extends BaseDTO<?>> dto){
		Class<? extends BaseBO> type = null;
		//Pide el tipo primitivo de mi superclase
		ParameterizedType typeParameterized = (ParameterizedType) dto.getGenericSuperclass();
		//Toma el tipo class de mi argumento que tiene BaseDTO
		type = (Class<? extends BaseBO>) typeParameterized.getActualTypeArguments()[0];
		//Retorna el tipo class
		return type;
	}

}
