/**
 * 
 */
package com.elioth010.generic.bs.dao.exception;

/**
 * @author elioth010 @date Oct 24, 2015 12:59:08 PM 
 * this class throw all hibernate exception occurred when 
 * the AbstractDAO is Called
 */
public class ORMException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7344612336351645758L;

	/**
	 * 
	 */
	public ORMException() {
		super();
	}

	/**
	 * @param message
	 */
	public ORMException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ORMException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ORMException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ORMException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
