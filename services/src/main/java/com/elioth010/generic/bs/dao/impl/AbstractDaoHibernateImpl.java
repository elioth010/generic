package com.elioth010.generic.bs.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.DataException;
import org.hibernate.exception.JDBCConnectionException;
import org.hibernate.exception.SQLGrammarException;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.elioth010.generic.bs.dao.AbstractDaoHibernate;
import com.elioth010.generic.bs.dao.exception.ORMException;
import com.elioth010.generic.eis.bo.base.BaseBO;
import com.elioth010.generic.util.persistence.ListaParametrosDTO;
import com.elioth010.generic.util.persistence.Parametro;

/**
 * @author developer
 */
@Repository
public class AbstractDaoHibernateImpl implements AbstractDaoHibernate{
	
	protected SessionFactory sessionFactory;
	
	private static final String FROM = "from";

	@Override
	public <T> BaseBO save(BaseBO baseEntity) throws ORMException{
		try{
			this.getSession().save(baseEntity);
			return baseEntity;
		}catch(DataException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(SQLGrammarException e){
			e.printStackTrace();
			throw new ORMException("Check your SQL sintax because thow error: "+e.getMessage(), e.getCause());
		}catch(ConstraintViolationException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(JDBCConnectionException e){
			e.printStackTrace();
			throw new ORMException("The database connection fails!! Check the StackTrace", e.getCause());
		}catch(HibernateException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}
	}

	@Override
	public void update(BaseBO baseEntity) throws ORMException{
		try{
			this.getSession().update(baseEntity);
		}catch(DataException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(SQLGrammarException e){
			e.printStackTrace();
			throw new ORMException("Check your SQL sintax because thow error: "+e.getMessage(), e.getCause());
		}catch(ConstraintViolationException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(JDBCConnectionException e){
			e.printStackTrace();
			throw new ORMException("The database connection fails!! Check the StackTrace", e.getCause());
		}catch(HibernateException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}
	}

	@Override
	public void delete(BaseBO baseEntity) throws ORMException{
		try{
			this.getSession().delete(baseEntity);
		}catch(DataException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(SQLGrammarException e){
			e.printStackTrace();
			throw new ORMException("Check your SQL sintax because thow error: "+e.getMessage(), e.getCause());
		}catch(ConstraintViolationException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(JDBCConnectionException e){
			e.printStackTrace();
			throw new ORMException("The database connection fails!! Check the StackTrace", e.getCause());
		}catch(HibernateException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}
	}

	@Override
	public <T> BaseBO findWithGet(Class<? extends BaseBO> clazz, Integer id) throws ORMException{
		try{
			return (BaseBO) this.getSession().get(clazz, id);
		}catch(DataException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(SQLGrammarException e){
			e.printStackTrace();
			throw new ORMException("Check your SQL sintax because thow error: "+e.getMessage(), e.getCause());
		}catch(ConstraintViolationException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(JDBCConnectionException e){
			e.printStackTrace();
			throw new ORMException("The database connection fails!! Check the StackTrace", e.getCause());
		}catch(HibernateException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}
	}

	@Override
	public <T> BaseBO findWithLoad(Class<? extends BaseBO> clazz, Integer id) throws ORMException{
		try{
			return (BaseBO) this.getSession().load(clazz, id);
		}catch(DataException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(SQLGrammarException e){
			e.printStackTrace();
			throw new ORMException("Check your SQL sintax because thow error: "+e.getMessage(), e.getCause());
		}catch(ConstraintViolationException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(JDBCConnectionException e){
			e.printStackTrace();
			throw new ORMException("The database connection fails!! Check the StackTrace", e.getCause());
		}catch(HibernateException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public <T> List<BaseBO> findAll(Class<? extends BaseBO> clazz) throws ORMException{
		try{
			return this.getSession().createQuery(FROM + " " + clazz.getSimpleName()).list();
		}catch(DataException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(SQLGrammarException e){
			e.printStackTrace();
			throw new ORMException("Check your SQL sintax because thow error: "+e.getMessage(), e.getCause());
		}catch(ConstraintViolationException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(JDBCConnectionException e){
			e.printStackTrace();
			throw new ORMException("The database connection fails!! Check the StackTrace", e.getCause());
		}catch(HibernateException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public <T> List<BaseBO> find(BaseBO baseEntity) throws ORMException{
		try{
			Criteria criteria = this.getSession().createCriteria(baseEntity.getClass());
			criteria.add(Example.create(baseEntity));
			return criteria.list();
		}catch(DataException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(SQLGrammarException e){
			e.printStackTrace();
			throw new ORMException("Check your SQL sintax because thow error: "+e.getMessage(), e.getCause());
		}catch(ConstraintViolationException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(JDBCConnectionException e){
			e.printStackTrace();
			throw new ORMException("The database connection fails!! Check the StackTrace", e.getCause());
		}catch(HibernateException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public <T> List<BaseBO> findByQuery(String query, Class<? extends BaseBO> clazz, ListaParametrosDTO parametros) throws ORMException{
		try{
			Query querySQL = this.getSession().getNamedQuery(query);
			List<Parametro> listParametros = parametros.getParametros();
			for (Parametro param : listParametros) {
				querySQL.setParameter(param.getName(), param.getValue());
			}
			return querySQL.list();
		}catch(DataException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(SQLGrammarException e){
			e.printStackTrace();
			throw new ORMException("Check your SQL sintax because thow error: "+e.getMessage(), e.getCause());
		}catch(ConstraintViolationException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(JDBCConnectionException e){
			e.printStackTrace();
			throw new ORMException("The database connection fails!! Check the StackTrace", e.getCause());
		}catch(HibernateException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public <T> List<BaseBO> findBySQLQuery(String query, Class<? extends BaseBO> clazz, ListaParametrosDTO parametros) throws ORMException{
		try{
			Query querySQL = this.getSession().createSQLQuery(query);
			for (Parametro param : parametros.getParametros()) {
				querySQL.setParameter(param.getName(), param.getValue(), param.getType());
			}
			querySQL.setResultTransformer(Transformers.aliasToBean(clazz));
			return querySQL.list();
		}catch(DataException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(SQLGrammarException e){
			e.printStackTrace();
			throw new ORMException("Check your SQL sintax because thow error: "+e.getMessage(), e.getCause());
		}catch(ConstraintViolationException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(JDBCConnectionException e){
			e.printStackTrace();
			throw new ORMException("The database connection fails!! Check the StackTrace", e.getCause());
		}catch(HibernateException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}
	}
	
	@Override
	public <T> BaseBO findById(Class<? extends BaseBO> clazz, Integer id) throws ORMException{
		try{
			return findWithGet(clazz, id);
		}catch(DataException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(SQLGrammarException e){
			e.printStackTrace();
			throw new ORMException("Check your SQL sintax because thow error: "+e.getMessage(), e.getCause());
		}catch(ConstraintViolationException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}catch(JDBCConnectionException e){
			e.printStackTrace();
			throw new ORMException("The database connection fails!! Check the StackTrace", e.getCause());
		}catch(HibernateException e){
			e.printStackTrace();
			throw new ORMException(e.getMessage(), e.getCause());
		}
	}
	
	public Session getSession() {
		return this.getSessionFactory().getCurrentSession();
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
