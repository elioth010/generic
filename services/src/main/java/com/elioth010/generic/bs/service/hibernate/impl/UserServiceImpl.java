/**
 * 
 */
package com.elioth010.generic.bs.service.hibernate.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.elioth010.generic.bs.dao.exception.ORMException;
import com.elioth010.generic.bs.service.hibernate.UserService;
import com.elioth010.generic.eis.dto.UserDTO;

/**
 * @author elioth010
 *
 */
public class UserServiceImpl extends AbstractServiceHibernateImpl implements UserService {
	
	private static final Logger logger  = Logger.getLogger(UserServiceImpl.class);
	
	@Override
	public UserDTO findUserByUsername(String username) {
		UserDTO user = new UserDTO();
		user.setUsername(username);
		try{
			@SuppressWarnings("unchecked")
			List<UserDTO> users = (List<UserDTO>) super.find(user);
			if(users!=null){
				if(!users.isEmpty()){
					return users.get(0);
				}
			}
		}catch(ORMException e){
			e.printStackTrace();
			logger.debug(e.getMessage(), e);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserDTO> findAllUsers() {
		try{
			return (List<UserDTO>) super.findAll(UserDTO.class);
		}catch(ORMException e){
			e.printStackTrace();
			logger.debug(e.getMessage(), e);
		}
		return new ArrayList<UserDTO>();
	}

}
