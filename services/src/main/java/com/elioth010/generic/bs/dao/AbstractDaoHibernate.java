package com.elioth010.generic.bs.dao;

import java.util.List;

import com.elioth010.generic.bs.dao.exception.ORMException;
import com.elioth010.generic.eis.bo.base.BaseBO;
import com.elioth010.generic.util.persistence.ListaParametrosDTO;

public interface AbstractDaoHibernate{
	
	public <T> BaseBO save(BaseBO baseEntity) throws ORMException;

	public void update(BaseBO baseEntity) throws ORMException;

	public void delete(BaseBO baseEntity) throws ORMException;

	public <T> List<BaseBO> find(BaseBO baseEntity) throws ORMException;

	public <T>BaseBO findById(Class<? extends BaseBO> clazz, Integer id) throws ORMException;

	public <T> List<BaseBO> findAll(Class<? extends BaseBO> clazz) throws ORMException;
	
	public <T>BaseBO findWithGet(Class<? extends BaseBO> clazz, Integer id) throws ORMException;
	
	public <T>BaseBO findWithLoad(Class<? extends BaseBO> clazz, Integer id) throws ORMException;
	
	public <T> List<BaseBO> findByQuery(String query, Class<? extends BaseBO> clazz, ListaParametrosDTO parametros) throws ORMException;

	public <T> List<BaseBO> findBySQLQuery(String query, Class<? extends BaseBO> clazz, ListaParametrosDTO parametros) throws ORMException;
}
