package com.elioth010.generic.security.dao.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.elioth010.generic.security.eis.dto.SessionDTO;

public class SessionsRepository implements Runnable {

	protected static final Logger logger = Logger.getLogger(SessionsRepository.class);
	private Map<String, SessionDTO> sessions;
	private static SessionsRepository currentInstance;
	private Integer sessionTimeout;

	/** Intervalo de tiempo en el que se verifica, si las sesiones ya expiraron */
	private static final Integer THREAD_SLEEP = 60000;

	/**
	 * Constructor que utiliza el timeout parametrizado
	 */
	public SessionsRepository(Integer timeout) {
		this.sessionTimeout = timeout;
		sessions = new HashMap<String, SessionDTO>();
		currentInstance = this;
		Thread runner = new Thread(this, "GenericSessionsRepositoryThread");
		runner.setDaemon(true);
		runner.start();
	}

	/**
	 * Metodo que se encarga de crear una nueva sesion
	 */
	public void createSession(SessionDTO session) {
		for (;;) {
			synchronized (sessions) {
				if (sessions.get(session.getToken()) != null)
					continue;
				else {
					sessions.put(session.getToken(), session);
					return;
				}
			}
		}
	}

	/**
	 * Metodo que se encarga de obtener una sesion
	 */
	public SessionDTO getSession(String token, String ip) {
		synchronized (sessions) {
			SessionDTO s = sessions.get(token);
			if (s != null && s.getIp().equals(ip)) {
				return s;
			} else {
				return null;
			}
		}
	}

	/**
	 * Metodo que se encarga de obtener las sesiones del usuario
	 */
	public List<SessionDTO> getSessionsByUsername(String username) {
		synchronized (sessions) {
			List<SessionDTO> sessionsByUsername = new LinkedList<SessionDTO>();
			for (Map.Entry<String, SessionDTO> info : sessions.entrySet()) {
				if (info.getValue().getUsername().equals(username)) {
					sessionsByUsername.add(info.getValue());
				}
			}
			return sessionsByUsername;
		}
	}

	/**
	 * Metodo que sobreescribe una sesion
	 */
	public void updateSession(SessionDTO session) {
		synchronized (sessions) {
			sessions.put(session.getToken(), session);
		}
	}

	/**
	 * Metodo que elimina una sesion
	 */
	public void deleteSession(SessionDTO session) {
		synchronized (sessions) {
			try {
				sessions.remove(session.getToken());
			} catch (Exception e) {
				logger.error("No se ha podido eliminar la sesion: " + session.getToken(), e);
			}

		}
	}

	public void run() {
		for (;;) {
			try {
				Thread.sleep(THREAD_SLEEP);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			long currentMillis = System.currentTimeMillis();
			LinkedList<String> expiredList = new LinkedList<String>();
			synchronized (sessions) {
				for (Map.Entry<String, SessionDTO> info : sessions.entrySet()) {
					if ((info.getValue().getTimeMillis() + this.sessionTimeout) <= currentMillis) {
						expiredList.add(info.getKey());
					}
				}
				for (String key : expiredList) {
				//	DESCOMENTAR PARA VENCER TOKEN
					logger.info("SessionsRepository.thread - remove session: " + key);
					sessions.remove(key);
				}
				expiredList.clear();
				expiredList = null;
			}
		}
	}

	public static SessionsRepository getInstance() {
		return currentInstance;
	}

	public Integer getSessionTimeout() {
		return sessionTimeout;
	}

	public void setSessionTimeout(Integer sessionTimeout) {
		this.sessionTimeout = sessionTimeout;
	}
}
