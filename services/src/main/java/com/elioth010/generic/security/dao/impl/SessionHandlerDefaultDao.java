package com.elioth010.generic.security.dao.impl;

import java.util.List;

import com.elioth010.generic.security.dao.SessionHandlerDao;
import com.elioth010.generic.security.eis.dto.SessionDTO;

public class SessionHandlerDefaultDao implements SessionHandlerDao {

	@Override
	public void createSession(SessionDTO sessionDTO) {
		SessionsRepository.getInstance().createSession(sessionDTO);
	}

	@Override
	public List<SessionDTO> getSessionsByUsername(String username) {
		return SessionsRepository.getInstance().getSessionsByUsername(username);
	}

	@Override
	public SessionDTO getSession(String sessionToken, String ipAddress) {
		return SessionsRepository.getInstance().getSession(sessionToken, ipAddress);
	}

	@Override
	public void updateSession(SessionDTO session) {
		SessionsRepository.getInstance().updateSession(session);
	}

	@Override
	public void deleteSession(SessionDTO sessionDTO) {
		SessionsRepository.getInstance().deleteSession(sessionDTO);
	}
}
