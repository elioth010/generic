package com.elioth010.generic.security.dao;

import java.util.List;

import com.elioth010.generic.security.eis.dto.SessionDTO;

public interface SessionHandlerDao {

	public void createSession(SessionDTO sessionDTO);

	public List<SessionDTO> getSessionsByUsername(String username);

	public SessionDTO getSession(String sessionToken, String ipAddress);

	public void updateSession(SessionDTO session);

	public void deleteSession(SessionDTO sessionDTO);

}
