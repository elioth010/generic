package com.elioth010.generic.eis.dto;

import java.io.Serializable;
import java.util.List;

public class SheetDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8038499094507703418L;

	private int rowCount;
	private List<RowDTO> rows;
	private List<Float> columnWidth;
	private String sheetName;
	private Integer sheetNumber;

	public SheetDTO() {
		super();
	}

	public SheetDTO(int rowCount, List<RowDTO> rows, List<Float> columnsWidth) {
		super();
		this.rowCount = rowCount;
		this.rows = rows;
		this.columnWidth = columnsWidth;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	public List<RowDTO> getRows() {
		return rows;
	}

	public void setRows(List<RowDTO> rows) {
		this.rows = rows;
	}

	public List<Float> getColumnWidth() {
		return columnWidth;
	}

	public void setColumnWidth(List<Float> columnWidth) {
		this.columnWidth = columnWidth;
	}

	public String getSheetName() {
		return sheetName;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	public Integer getSheetNumber() {
		return sheetNumber;
	}

	public void setSheetNumber(Integer sheetNumber) {
		this.sheetNumber = sheetNumber;
	}
	
}
