package com.elioth010.generic.eis.dto;

import java.io.Serializable;
import java.util.List;

public class BookDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6143405977121583323L;
	private List<SheetDTO> sheets;

	public BookDTO() {
		super();
	}

	public BookDTO(List<SheetDTO> sheets) {
		super();
		this.sheets = sheets;
	}

	public List<SheetDTO> getSheets() {
		return sheets;
	}

	public void setSheets(List<SheetDTO> sheets) {
		this.sheets = sheets;
	}

}
