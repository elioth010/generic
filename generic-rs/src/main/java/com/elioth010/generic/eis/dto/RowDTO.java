package com.elioth010.generic.eis.dto;

import java.io.Serializable;
import java.util.List;

public class RowDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6410937589480924945L;

	public RowDTO() {
		super();
	}

	public RowDTO(Integer numRow, List<String> row, float rowHeight) {
		super();
		this.numRow = numRow;
		this.row = row;
		this.rowHeight = rowHeight;
	}

	private Integer numRow;
	private List<String> row;
	private float rowHeight;

	public Integer getNumRow() {
		return numRow;
	}

	public void setNumRow(Integer numRow) {
		this.numRow = numRow;
	}

	public List<String> getRow() {
		return row;
	}

	public void setRow(List<String> row) {
		this.row = row;
	}

	public float getRowHeight() {
		return rowHeight;
	}

	public void setRowHeight(float rowHeight) {
		this.rowHeight = rowHeight;
	}
}
