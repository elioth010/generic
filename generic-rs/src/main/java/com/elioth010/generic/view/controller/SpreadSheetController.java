/**
 * 
 */
package com.elioth010.generic.view.controller;

import java.io.Serializable;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.elioth010.generic.eis.dto.BookDTO;
import com.elioth010.generic.eis.dto.SheetDTO;

/**
 * @author elioth010
 * 
 */

@RestController
@RequestMapping("/spreadsheet")
public class SpreadSheetController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 605410129388648308L;
	//private static final float POINT_TO_PIXEL_CONVERSION = 1.3333333336f;
	//private static final float MILLIMETER_TO_PIXEL_CONVERSION = 3.779527559f;
	private BookDTO bookDTO;

	private String excelJsonData;

	public SpreadSheetController() {
		super();
	}

	@RequestMapping(value = "data", method = RequestMethod.GET)
	public BookDTO loadExcelFile(@RequestParam(value="file") String fileName) {
		/*try {
			List<List<String>> data = new ArrayList<List<String>>();
			List<SheetDTO> sheetDTOs = new ArrayList<SheetDTO>();
			List<Float> rowsHeight= new ArrayList<Float>();
			SheetDTO sheetDTO = new SheetDTO();
			bookDTO = new BookDTO();
			List<RowDTO> rowDTOs = new ArrayList<RowDTO>();
			File file = new File("/home/elioth010/Documentos/uploadedfiles/"+fileName);
			FileInputStream excelFile = new FileInputStream(file);
			if (FilenameUtils.getExtension(file.getAbsolutePath()).equalsIgnoreCase("xlsx")) {
				XSSFWorkbook book = new XSSFWorkbook(excelFile);
				for(int n = 0 ; n<book.getNumberOfSheets(); n++){
					data = new ArrayList<List<String>>();
					XSSFSheet sheet = book.getSheetAt(n);
					rowDTOs = new ArrayList<RowDTO>();
					sheetDTO = new SheetDTO();
					System.out.println("Last Row Number " + sheet.getLastRowNum());
					sheetDTO.setSheetName(sheet.getSheetName());
					sheetDTO.setSheetNumber(n);
					sheetDTO.setRowCount(sheet.getLastRowNum());
					if(sheet.getLastRowNum() > 0){
						int maxColumn = 0;
						for(int i = 0; i<=sheet.getLastRowNum(); i++){
							Row row = sheet.getRow(i);
							if(row!=null){
								if(maxColumn<row.getLastCellNum()){
									maxColumn = row.getLastCellNum();							
								}
							}
						}
							
						data.add(initializateSheet(maxColumn));
						rowsHeight.add(sheet.getDefaultRowHeightInPoints()*POINT_TO_PIXEL_CONVERSION);
						List<Float> columnsWidth = new ArrayList<Float>();
						for(int i = 0; i< maxColumn; i++){
							columnsWidth.add(sheet.getColumnWidthInPixels(i));
						}
						sheetDTO.setColumnWidth(columnsWidth);
						
						for (int i = 0; i <= sheet.getLastRowNum(); i++) {
							Row row = sheet.getRow(i);
							List<String> list = new ArrayList<String>();
							if (row == null) {
								System.out.println("Empty Row");
								data.add(list);
								rowsHeight.add(sheet.getDefaultRowHeightInPoints()*POINT_TO_PIXEL_CONVERSION);
							} else {
								rowsHeight.add(row.getHeightInPoints()*POINT_TO_PIXEL_CONVERSION);
								for (int j = 0; j < row.getLastCellNum(); j++) {
									Cell cell = row.getCell(j);
									if (cell == null) {
										list.add("");
									} else {
										switch (cell.getCellType()) {
										case Cell.CELL_TYPE_NUMERIC:
											System.out.println(cell.getNumericCellValue()+ " Numeric");
											list.add(cell.getNumericCellValue() + "");
											break;
										case Cell.CELL_TYPE_STRING:
											System.out.println(cell.getStringCellValue() + " String");
											list.add(cell.getStringCellValue());
											break;
										case Cell.CELL_TYPE_FORMULA:
											System.out.println(cell.getCellFormula());
											list.add(cell.getCellFormula());
											break;
										case Cell.CELL_TYPE_BOOLEAN:
											System.out.println(cell.getBooleanCellValue());
											list.add(String.valueOf(cell.getBooleanCellValue()));
											break;
										default:
											break;
										}
									}
								}
								data.add(list);
							}
						}
						System.out.println("NUM OF ROWS PARSED :" + data.size());
						for (int i = 0; i < data.size(); i++) {
							List<String> row = data.get(i);
							rowDTOs.add(new RowDTO(i, row, rowsHeight.get(i)));
						}
						sheetDTO.setRows(rowDTOs);
						sheetDTOs.add(sheetDTO);
					}
				}
				book.close();
				excelFile.close();
				bookDTO.setSheets(sheetDTOs);
				ObjectMapper maper = new ObjectMapper();
				excelJsonData = maper.writeValueAsString(bookDTO);
				System.out.println(excelJsonData);
				System.out.println(excelFile);
				return bookDTO;
			} else if(FilenameUtils.getExtension(file.getAbsolutePath()).equalsIgnoreCase("xls")){
				HSSFWorkbook book = new HSSFWorkbook(excelFile);
				for(int n = 0 ; n<book.getNumberOfSheets(); n++){
					HSSFSheet sheet = book.getSheetAt(n);
					System.out.println("Last Row Number " + sheet.getLastRowNum());
					data = new ArrayList<List<String>>();
					rowDTOs = new ArrayList<RowDTO>();
					sheetDTO = new SheetDTO();
					sheetDTO.setRowCount(sheet.getLastRowNum());
					sheetDTO.setSheetNumber(n);
					sheetDTO.setSheetName(sheet.getSheetName());
					if(sheet.getLastRowNum() > 0){
						int maxColumn = 0;
						for(int i = 0; i<=sheet.getLastRowNum(); i++){
							Row row = sheet.getRow(i);
							if(row!=null){
								if(maxColumn<row.getLastCellNum()){
									maxColumn = row.getLastCellNum();							
								}
							}
						}
							
						data.add(initializateSheet(maxColumn));
						rowsHeight.add(sheet.getDefaultRowHeightInPoints()*POINT_TO_PIXEL_CONVERSION);
						List<Float> columnsWidth = new ArrayList<Float>();
						for(int i = 0; i< maxColumn; i++){
							columnsWidth.add(sheet.getColumnWidthInPixels(i));
						}
						sheetDTO.setColumnWidth(columnsWidth);
						
						for (int i = 0; i <= sheet.getLastRowNum(); i++) {
							Row row = sheet.getRow(i);
							List<String> list = new ArrayList<String>();
							if (row == null) {
								System.out.println("Empty Row");
								data.add(list);
								rowsHeight.add(sheet.getDefaultRowHeightInPoints()*POINT_TO_PIXEL_CONVERSION);
							} else {
								rowsHeight.add(row.getHeightInPoints()*POINT_TO_PIXEL_CONVERSION);
								for (int j = 0; j < row.getLastCellNum(); j++) {
									Cell cell = row.getCell(j);
									if (cell == null) {
										list.add("");
									} else {
										switch (cell.getCellType()) {
										case Cell.CELL_TYPE_NUMERIC:
											System.out.println(cell.getNumericCellValue()+ " Numeric");
											list.add(cell.getNumericCellValue() + "");
											break;
										case Cell.CELL_TYPE_STRING:
											System.out.println(cell.getStringCellValue() + " String");
											list.add(cell.getStringCellValue());
											break;
										case Cell.CELL_TYPE_FORMULA:
											System.out.println(cell.getCellFormula());
											list.add(cell.getCellFormula());
											break;
										case Cell.CELL_TYPE_BOOLEAN:
											System.out.println(cell.getBooleanCellValue());
											list.add(String.valueOf(cell.getBooleanCellValue()));
											break;
										default:
											break;
										}
									}
								}
								data.add(list);
							}
						}
						System.out.println("NUM OF ROWS PARSED :" + data.size());
						for (int i = 0; i < data.size(); i++) {
							List<String> row = data.get(i);
							rowDTOs.add(new RowDTO(i, row, rowsHeight.get(i)));
						}
						sheetDTO.setRows(rowDTOs);
						sheetDTOs.add(sheetDTO);
					}
				}
				book.close();
				excelFile.close();
				bookDTO.setSheets(sheetDTOs);
				ObjectMapper maper = new ObjectMapper();
				excelJsonData = maper.writeValueAsString(bookDTO);
				System.out.println(excelJsonData);
				System.out.println(excelFile);
				return bookDTO;
			}else if(FilenameUtils.getExtension(file.getAbsolutePath()).equalsIgnoreCase("ods") || FilenameUtils.getExtension(file.getAbsolutePath()).equalsIgnoreCase("ots")){
				try {
					SpreadsheetDocument book = SpreadsheetDocument.loadDocument(file);
					for(int n = 0 ; n<book.getSheetCount(); n++){
						Table sheet = book.getSheetByIndex(n);
						System.out.println("Last Row Number " + sheet.getRowCount());
						int rowCount = sheet.getRowCount();
						data = new ArrayList<List<String>>();
						rowDTOs = new ArrayList<RowDTO>();
						sheetDTO = new SheetDTO();
						sheetDTO.setRowCount(sheet.getRowCount());
						sheetDTO.setSheetNumber(n);
						sheetDTO.setSheetName(sheet.getTableName());
						if(rowCount > 0){
							int maxColumn = sheet.getColumnCount();
							data.add(initializateSheet(maxColumn));
							List<Float> columnsWidth = new ArrayList<Float>();
							for(int i = 0; i< maxColumn; i++){
								columnsWidth.add(Float.parseFloat((sheet.getColumnByIndex(i).getWidth()*MILLIMETER_TO_PIXEL_CONVERSION)+""));
							}
							sheetDTO.setColumnWidth(columnsWidth);
							rowsHeight.add(17f);
							for (int i = 0; i <= rowCount; i++) {
								org.odftoolkit.simple.table.Row row = sheet.getRowByIndex(i);
								List<String> list = new ArrayList<String>();
								
								if (row == null) {
									System.out.println("Empty Row");
									data.add(list);
									rowsHeight.add(17f);
								} else {
									rowsHeight.add(Float.parseFloat(row.getHeight()*MILLIMETER_TO_PIXEL_CONVERSION+""));
									for (int j = 0; j < row.getCellCount(); j++) {
										org.odftoolkit.simple.table.Cell cell = row.getCellByIndex(j);
										if (cell == null) {
											list.add("");
										} else {
											list.add(cell.getStringValue());
										}
									}
									data.add(list);
								}
							}
							System.out.println("NUM OF ROWS PARSED :" + data.size());
							for (int i = 0; i < data.size(); i++) {
								List<String> row = data.get(i);
								rowDTOs.add(new RowDTO(i, row, rowsHeight.get(i)));
							}
							sheetDTO.setRows(rowDTOs);
							sheetDTOs.add(sheetDTO);
						}
					}
					book.close();
					excelFile.close();
					bookDTO.setSheets(sheetDTOs);
					ObjectMapper maper = new ObjectMapper();
					excelJsonData = maper.writeValueAsString(bookDTO);
					System.out.println(excelJsonData);
					System.out.println(excelFile);
					return bookDTO;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			excelFile.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		return new BookDTO(new ArrayList<SheetDTO>());
	}

	/*private List<String> initializateSheet(int maxColumn) {
		List<String> data = new ArrayList<String>();
		for(int i=0; i<maxColumn; i++){
			data.add("");
		}
		return data;
	}*/
	
	@RequestMapping(value = "last", method = RequestMethod.GET)
	public BookDTO getLastLoadedBook() {
		return bookDTO;
	}

	public String getExcelJsonData() {
		return excelJsonData;
	}

	public void setExcelJsonData(String excelJsonData) {
		this.excelJsonData = excelJsonData;
	}

}
