/**
 * 
 */
package com.elioth010.generic.view.controller;

import java.nio.file.AccessDeniedException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.elioth010.generic.api.error.ApiErrorCode;
import com.elioth010.generic.api.error.RestApiError;
import com.elioth010.generic.bs.service.hibernate.UserService;
import com.elioth010.generic.eis.dto.UserDTO;

/**
 * @author elioth010
 *
 */
@RestController
@RequestMapping("/api/test")
public class ExampleOAuth2 implements InitializingBean {

	private static final Logger logger = LoggerFactory.getLogger(ExampleOAuth2.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	
	@Autowired
	@Qualifier(value="userHibernateService")
	private UserService userHibernateService;

	@Override
	public void afterPropertiesSet() throws Exception {

	}

	/**
	 * @param filter
	 *            If not null, filter to be applied. Example:
	 *            q?title:titulo1,content:Contenido1
	 * @param fields
	 *            If not null. Partial response. Example: ?fields=id,title
	 * @return Note list of the logged user.
	 */
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	@PreAuthorize("#oauth2.isUser() and #oauth2.clientHasRole('ROLE_USER') and #oauth2.hasScope('trust')")
	@ResponseBody
	public List<?> getUsers(@RequestParam(required = false, value = "q") String filter, @RequestParam(required = false, value = "fields") String fields) {
		logger.debug("Api Users .getUsers . filter {}, fields {}",filter ,fields);

		UserDTO user = this.getLoggedUser();
		logger.debug("Username logged {}", user.getUsername());
		//Map<String, Object> params = null;

		// if (StringUtils.isNotBlank(filter)){
		// params = SmallNotesApiHelper.parseFilter(filter);
		// }

		List<UserDTO> users = this.getUserHibernateService().findAllUsers();//.getAll(user, params);

		/*
		 * if (logger.isDebugEnabled()){ int numNotes = 0; if (notes != null){
		 * numNotes = notes.size(); }
		 * logger.debug("num notes that match filter {}"+numNotes); }
		 */

		// ¿Is partial response requested?
		/*
		 * if (StringUtils.isNotBlank(fields) && (notes != null)){ return
		 * SmallNotesApiHelper.applyPartialResponse(notes, fields); } else {
		 * return notes; }
		 */
		return users;
	}

	private UserDTO getLoggedUser() {
		String username = null;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Object principal = authentication.getPrincipal();

		if (principal instanceof UserDetails) {
			username = ((UserDetails) principal).getUsername();
		} else if (principal instanceof UsernamePasswordAuthenticationToken) {
			username = ((UsernamePasswordAuthenticationToken) principal).getName();
		} else if (principal instanceof OAuth2Authentication) {
			username = ((OAuth2Authentication) principal).getUserAuthentication().getName();
		}

		logger.debug("Logged user username " + username);

		UserDTO user = this.userHibernateService.findUserByUsername(username);

		logger.debug("Logged user {} " + "");

		return user;
	}

	/**
	 * Exception Handlers
	 */

	@ExceptionHandler(Exception.class)
	protected @ResponseBody ResponseEntity<RestApiError> handleNoteNotFoundException(Exception noteNotFoundException, HttpServletRequest request, HttpServletResponse response) {
		RestApiError restApiError = new RestApiError(HttpStatus.NOT_FOUND, ApiErrorCode.RESOURCE_NOTE_FOUND, noteNotFoundException.getMessage(), noteNotFoundException.getMessage(),
				ApiErrorCode.RESOURCE_NOTE_FOUND.getValue());
		return this.createAndSendResponse(restApiError);
	}

	@ExceptionHandler(AccessDeniedException.class)
	protected @ResponseBody ResponseEntity<RestApiError> handleAccessDeniedException(AccessDeniedException accessDeniedException, HttpServletRequest request, HttpServletResponse response) {
		RestApiError restApiError = new RestApiError(HttpStatus.UNAUTHORIZED, ApiErrorCode.ACCESS_DENIED, accessDeniedException.getMessage(), accessDeniedException.getMessage(),
				ApiErrorCode.ACCESS_DENIED.getValue());
		return this.createAndSendResponse(restApiError);
	}

	@ExceptionHandler(SecurityException.class)
	protected @ResponseBody ResponseEntity<RestApiError> handleSecurityException(SecurityException exception, HttpServletRequest request, HttpServletResponse response) {
		RestApiError restApiError = new RestApiError(HttpStatus.UNAUTHORIZED, ApiErrorCode.SECURITY, exception.getMessage(), exception.getMessage(), ApiErrorCode.SECURITY.getValue());
		return this.createAndSendResponse(restApiError);
	}

//	@ExceptionHandler(Exception.class)
//	protected @ResponseBody ResponseEntity<RestApiError> handleException(Exception exception, HttpServletRequest request, HttpServletResponse response) {
//		RestApiError restApiError = new RestApiError(HttpStatus.BAD_REQUEST, ApiErrorCode.GENERIC, exception.getMessage(), exception.getMessage(), ApiErrorCode.GENERIC.getValue());
//		return this.createAndSendResponse(restApiError);
//	}

	public ResponseEntity<RestApiError> createAndSendResponse(RestApiError restApiError) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Cache-Control", "no-store");
		headers.set("Pragma", "no-cache");
		headers.setContentType(MediaType.APPLICATION_JSON);
		ResponseEntity<RestApiError> responseEntity = new ResponseEntity<RestApiError>(restApiError, headers, restApiError.getHttpStatusCode());
		return responseEntity;
	}

	public UserService getUserHibernateService() {
		return userHibernateService;
	}

	public void setUserHibernateService(UserService userHibernateService) {
		this.userHibernateService = userHibernateService;
	}

}
