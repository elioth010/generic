package com.elioth010.generic.api.error;

public enum ApiErrorCode {
	RESOURCE_NOTE_FOUND("Recurso no encontrado"), ACCESS_DENIED("Acceso denegado a recurso"), SECURITY("Error de seguridad"), GENERIC("ERROR DESCONOCIDO");

	private String value;

	private ApiErrorCode(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
