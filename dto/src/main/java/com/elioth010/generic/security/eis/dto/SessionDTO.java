package com.elioth010.generic.security.eis.dto;

public class SessionDTO {

	private String token;
	private String username;
	private String ip;
	private Long timeMillis;

	public SessionDTO() {
		super();
	}

	public SessionDTO(String username, String ip) {
		super();
		this.username = username;
		this.ip = ip;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Long getTimeMillis() {
		return timeMillis;
	}

	public void setTimeMillis(Long timeMillis) {
		this.timeMillis = timeMillis;
	}

}