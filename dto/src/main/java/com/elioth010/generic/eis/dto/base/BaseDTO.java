package com.elioth010.generic.eis.dto.base;

import java.io.Serializable;

import com.elioth010.generic.eis.bo.base.BaseBO;

/**
 * 
 * @author elioth010 created: 4/12/2014 
 * Using Eclipse IDE with Spring Framework
 *
 * @param <BASE>
 */
public class BaseDTO<BASE extends BaseBO> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1475922734837259120L;
	
	private Integer id;

	public BaseDTO() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
