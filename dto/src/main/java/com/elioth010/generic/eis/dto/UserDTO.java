package com.elioth010.generic.eis.dto;

import java.util.Date;

import com.elioth010.generic.eis.bo.User;
import com.elioth010.generic.eis.dto.base.BaseDTO;

/**
 * 
 * @author elioth010 @date Oct 16, 2015 7:22:29 PM
 *
 */
public class UserDTO extends BaseDTO<User> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5262001072956234607L;

	private Integer id;
	private Short version;
	private String username;
	private String password;
	private String email;
	private Boolean enabled;
	private Boolean expiredAccount;
	private Boolean lockedAccount;
	private Boolean expiredCredentials;
	private Short loginAttempts;
	private Date lastLogin;
	private Date lastPasswordChange;
	private Date created;
	private Date modified;
	private String lastIP;
	private String defaultProfileName;
	private String firstName;
	private String secondName;
	private String firstLastName;
	private String secondLastName;
	private Date lastPasswordReset;
	private Boolean passwordReset;
	private Boolean passwordExpires;

	public UserDTO() {
		super();
	}

	public UserDTO(Integer id, Short version, String username, String password, String email, Boolean enabled, Boolean expiredAccount, Boolean lockedAccount, Boolean expiredCredentials,
			Short loginAttempts, Date lastLogin, Date lastPasswordChange, Date created, Date modified, String lastIP, String defaultProfileName, String firstName, String secondName,
			String firstLastName, String secondLastName, Date lastPasswordReset, Boolean passwordReset, Boolean passwordExpires) {
		super();
		this.id = id;
		this.version = version;
		this.username = username;
		this.password = password;
		this.email = email;
		this.enabled = enabled;
		this.expiredAccount = expiredAccount;
		this.lockedAccount = lockedAccount;
		this.expiredCredentials = expiredCredentials;
		this.loginAttempts = loginAttempts;
		this.lastLogin = lastLogin;
		this.lastPasswordChange = lastPasswordChange;
		this.created = created;
		this.modified = modified;
		this.lastIP = lastIP;
		this.defaultProfileName = defaultProfileName;
		this.firstName = firstName;
		this.secondName = secondName;
		this.firstLastName = firstLastName;
		this.secondLastName = secondLastName;
		this.lastPasswordReset = lastPasswordReset;
		this.passwordReset = passwordReset;
		this.passwordExpires = passwordExpires;
	}

	public UserDTO(Short version, String username, String password, String email, Boolean enabled, Boolean expiredAccount, Boolean lockedAccount, Boolean expiredCredentials, Short loginAttempts,
			Date lastLogin, Date lastPasswordChange, Date created, Date modified, String lastIP, String defaultProfileName, String firstName, String secondName, String firstLastName,
			String secondLastName, Date lastPasswordReset, Boolean passwordReset, Boolean passwordExpires) {
		super();
		this.version = version;
		this.username = username;
		this.password = password;
		this.email = email;
		this.enabled = enabled;
		this.expiredAccount = expiredAccount;
		this.lockedAccount = lockedAccount;
		this.expiredCredentials = expiredCredentials;
		this.loginAttempts = loginAttempts;
		this.lastLogin = lastLogin;
		this.lastPasswordChange = lastPasswordChange;
		this.created = created;
		this.modified = modified;
		this.lastIP = lastIP;
		this.defaultProfileName = defaultProfileName;
		this.firstName = firstName;
		this.secondName = secondName;
		this.firstLastName = firstLastName;
		this.secondLastName = secondLastName;
		this.lastPasswordReset = lastPasswordReset;
		this.passwordReset = passwordReset;
		this.passwordExpires = passwordExpires;
	}

	public UserDTO(String username, String password, String email, Boolean enabled, Boolean expiredAccount, Boolean lockedAccount, Boolean expiredCredentials, Short loginAttempts, Date lastLogin,
			Date lastPasswordChange, Date created, Date modified, String lastIP, String defaultProfileName, String firstName, String secondName, String firstLastName, String secondLastName,
			Date lastPasswordReset, Boolean passwordReset, Boolean passwordExpires) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.enabled = enabled;
		this.expiredAccount = expiredAccount;
		this.lockedAccount = lockedAccount;
		this.expiredCredentials = expiredCredentials;
		this.loginAttempts = loginAttempts;
		this.lastLogin = lastLogin;
		this.lastPasswordChange = lastPasswordChange;
		this.created = created;
		this.modified = modified;
		this.lastIP = lastIP;
		this.defaultProfileName = defaultProfileName;
		this.firstName = firstName;
		this.secondName = secondName;
		this.firstLastName = firstLastName;
		this.secondLastName = secondLastName;
		this.lastPasswordReset = lastPasswordReset;
		this.passwordReset = passwordReset;
		this.passwordExpires = passwordExpires;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Short getVersion() {
		return version;
	}

	public void setVersion(Short version) {
		this.version = version;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getExpiredAccount() {
		return expiredAccount;
	}

	public void setExpiredAccount(Boolean expiredAccount) {
		this.expiredAccount = expiredAccount;
	}

	public Boolean getLockedAccount() {
		return lockedAccount;
	}

	public void setLockedAccount(Boolean lockedAccount) {
		this.lockedAccount = lockedAccount;
	}

	public Boolean getExpiredCredentials() {
		return expiredCredentials;
	}

	public void setExpiredCredentials(Boolean expiredCredentials) {
		this.expiredCredentials = expiredCredentials;
	}

	public Short getLoginAttempts() {
		return loginAttempts;
	}

	public void setLoginAttempts(Short loginAttempts) {
		this.loginAttempts = loginAttempts;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public Date getLastPasswordChange() {
		return lastPasswordChange;
	}

	public void setLastPasswordChange(Date lastPasswordChange) {
		this.lastPasswordChange = lastPasswordChange;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public String getLastIP() {
		return lastIP;
	}

	public void setLastIP(String lastIP) {
		this.lastIP = lastIP;
	}

	public String getDefaultProfileName() {
		return defaultProfileName;
	}

	public void setDefaultProfileName(String defaultProfileName) {
		this.defaultProfileName = defaultProfileName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getFirstLastName() {
		return firstLastName;
	}

	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public Date getLastPasswordReset() {
		return lastPasswordReset;
	}

	public void setLastPasswordReset(Date lastPasswordReset) {
		this.lastPasswordReset = lastPasswordReset;
	}

	public Boolean getPasswordReset() {
		return passwordReset;
	}

	public void setPasswordReset(Boolean passwordReset) {
		this.passwordReset = passwordReset;
	}

	public Boolean getPasswordExpires() {
		return passwordExpires;
	}

	public void setPasswordExpires(Boolean passwordExpires) {
		this.passwordExpires = passwordExpires;
	}

}
