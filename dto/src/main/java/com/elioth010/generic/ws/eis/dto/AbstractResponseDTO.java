package com.elioth010.generic.ws.eis.dto;

import javax.xml.bind.annotation.XmlTransient;

@XmlTransient
public abstract class AbstractResponseDTO {

	protected HeaderResponseDTO header = new HeaderResponseDTO();

	public AbstractResponseDTO(){
		super();
	}
	
	public AbstractResponseDTO(HeaderResponseDTO header){
		this.header = header; 
	}

	public HeaderResponseDTO getHeader() {
		return header;
	}

	public void setHeader(HeaderResponseDTO header) {
		this.header = header;
	}
}
