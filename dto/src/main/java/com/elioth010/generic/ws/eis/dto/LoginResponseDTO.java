package com.elioth010.generic.ws.eis.dto;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "header", "usuarioWS", "ip" })
@XmlRootElement(name = "LoginResponse", namespace = "http://ws.view.genericws.elioth.com/")
public class LoginResponseDTO extends AbstractResponseDTO {

	private String usuarioWS;
	private String ip;

	public String getUsuarioWS() {
		return usuarioWS;
	}

	public void setUsuarioWS(String usuarioWS) {
		this.usuarioWS = usuarioWS;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

}
