package com.elioth010.generic.ws.eis.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "sessionID" })
@XmlRootElement
public class HeaderRequestDTO {

	private String sessionID;

	@XmlElement(required = true)
	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
}
