package com.elioth010.generic.ws.eis.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "usuarioWS", "passwordWS" })
@XmlRootElement(name = "LoginRequest", namespace = "http://ws.view.genericws.elioth.com/")
public class LoginRequestDTO extends AbstractRequestDTO {

	private String usuarioWS;
	private String passwordWS;

	@XmlElement(required = true)
	public String getUsuarioWS() {
		return usuarioWS;
	}

	public void setUsuarioWS(String usuarioWS) {
		this.usuarioWS = usuarioWS;
	}

	@XmlElement(required = true)
	public String getPasswordWS() {
		return passwordWS;
	}

	public void setPasswordWS(String passwordWS) {
		this.passwordWS = passwordWS;
	}

	@XmlTransient
	public HeaderRequestDTO getHeader() {
		return header;
	}

}
