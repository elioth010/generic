package com.elioth010.generic.ws.eis.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlTransient
public abstract class AbstractRequestDTO {

	protected HeaderRequestDTO header = new HeaderRequestDTO();

	@XmlElement(required = true)
	public HeaderRequestDTO getHeader() {
		return header;
	}

	public void setHeader(HeaderRequestDTO header) {
		this.header = header;
	}

}
