package com.elioth010.generic.ws.eis.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "hora", "codigoError", "descripcionError", "referencia", "sesionID" })
@XmlRootElement
public class HeaderResponseDTO {

	private Date hora;
	private String sesionID;

	private Integer codigoError = 0;
	private String descripcionError = "";
	private String referencia;

	public HeaderResponseDTO() {
		super();
	}

	public HeaderResponseDTO(Integer codigoError, String descripcionError) {
		super();
		this.codigoError = codigoError;
		this.descripcionError = descripcionError;
	}

	public Date getHora() {
		return hora;
	}

	public void setHora(Date hora) {
		this.hora = hora;
	}

	public Integer getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(Integer codigoError) {
		if (codigoError == null) {
			this.codigoError = 0;
		} else {
			this.codigoError = codigoError;
		}
	}

	public String getDescripcionError() {
		return descripcionError;
	}

	public void setDescripcionError(String descripcionError) {
		if (descripcionError == null) {
			this.descripcionError = "";
		} else {
			this.descripcionError = descripcionError;
		}
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getSesionID() {
		return sesionID;
	}

	public void setSesionID(String sesionID) {
		this.sesionID = sesionID;
	}

}
