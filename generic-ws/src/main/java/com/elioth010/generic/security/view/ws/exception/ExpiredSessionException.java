package com.elioth010.generic.security.view.ws.exception;

public class ExpiredSessionException extends WebServiceException {

	private static final long serialVersionUID = -587441389936146044L;

	private static final String MESSAGE = "ExpiredSessionException";
	private static final Integer CODIGO = 990;

	public ExpiredSessionException() {
		super(CODIGO, MESSAGE);
	}

}