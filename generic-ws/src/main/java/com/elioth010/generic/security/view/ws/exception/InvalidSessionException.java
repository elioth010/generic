package com.elioth010.generic.security.view.ws.exception;


public class InvalidSessionException extends WebServiceException {

	private static final long serialVersionUID = -5254783262250159014L;

	private static final String MESSAGE = "InvalidSessionException";
	private static final Integer CODIGO = 991;

	public InvalidSessionException() {
		super(CODIGO, MESSAGE);
	}
}