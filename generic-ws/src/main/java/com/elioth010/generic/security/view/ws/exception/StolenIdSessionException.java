package com.elioth010.generic.security.view.ws.exception;

public class StolenIdSessionException extends WebServiceException {

	private static final long serialVersionUID = -6106397526516105023L;

	private static final String MESSAGE = "StolenIdSessionException";
	private static final Integer CODIGO = 992;

	public StolenIdSessionException() {
		super(CODIGO, MESSAGE);
	}
}