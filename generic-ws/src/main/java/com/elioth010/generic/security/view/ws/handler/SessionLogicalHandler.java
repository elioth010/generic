package com.elioth010.generic.security.view.ws.handler;

import javax.xml.bind.JAXBContext;
import javax.xml.ws.LogicalMessage;
import javax.xml.ws.handler.LogicalHandler;
import javax.xml.ws.handler.LogicalMessageContext;
import javax.xml.ws.handler.MessageContext;

import com.elioth010.generic.security.eis.dto.SessionDTO;
import com.elioth010.generic.security.service.SessionHandlerService;
import com.elioth010.generic.security.view.ws.exception.WebServiceException;
import com.elioth010.generic.ws.eis.dto.AbstractResponseDTO;
import com.elioth010.generic.ws.eis.dto.LoginResponseDTO;

public class SessionLogicalHandler implements LogicalHandler<LogicalMessageContext> {

	private static final JAXBContext JAXB_CONTEXT;
	protected static final Integer NON_ERROR = 0;
	protected static final Integer ERROR_CHANGE_PASSWORD = 104;
	protected static final Integer UNKNOWN_ERROR_CODE = 999;
	protected static final String UNKNOWN_ERROR_DESC = "Unknown";

	private SessionHandlerService sessionHandlerService;

	static {
		JAXBContext context;
		try {
			context = JAXBContext.newInstance("com.elioth010.generic.ws.eis.dto");
		} catch (Exception e) {
			e.printStackTrace();
			context = null;
		}
		JAXB_CONTEXT = context;
	}

	@Override
	public boolean handleMessage(LogicalMessageContext context) {
		Boolean outboundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		LogicalMessage lm = context.getMessage();
		if (!outboundProperty) {
			/** MENSAJE DE ENTRADA */
		} else {
			/** MENSAJE DE SALIDA */
			AbstractResponseDTO response = (AbstractResponseDTO) lm.getPayload(JAXB_CONTEXT);
			if (this.isSuccessful(response)) {
				/** RESPUESTA EXITOSA */
				try {
					if (response instanceof LoginResponseDTO) {
						String sessionID = this.createSession((LoginResponseDTO) response);
						((LoginResponseDTO) response).getHeader().setSesionID(sessionID);
						lm.setPayload(response, JAXB_CONTEXT);
					} // else if (response instanceof LogoutResponseDTO) {
						// this.killSession((LogoutResponseDTO) response);
					// }
				} catch (WebServiceException e) {
					e.printStackTrace();
					response.getHeader().setCodigoError(e.getCodigo());
					response.getHeader().setDescripcionError(e.getDescripcion());
					lm.setPayload(response, JAXB_CONTEXT);
				}
			}
		}
		return true;
	}

	@Override
	public boolean handleFault(LogicalMessageContext context) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void close(MessageContext context) {
		// TODO Auto-generated method stub
	}

	private String createSession(final LoginResponseDTO response) throws WebServiceException {
		return this.sessionHandlerService.createSession(new SessionDTO(response.getUsuarioWS(), response.getIp()));
	}

	// private SessionDTO killSession(final LogoutResponseDTO response) throws
	// WebServiceException {
	// return
	// this.sessionHandlerService.killSession(response.getHeader().getSesionID(),
	// response.getHeader().getIp());
	// }

	protected boolean isSuccessful(AbstractResponseDTO response) {
		boolean success = response.getHeader().getCodigoError() == null || NON_ERROR.equals(response.getHeader().getCodigoError());
		if (!success && response instanceof LoginResponseDTO && ERROR_CHANGE_PASSWORD.equals(response.getHeader().getCodigoError())) {
			/** CAMBIO CONTRASENA REQUERIDO */
			success = true;
		}
		return success;
	}

	public SessionHandlerService getSessionHandlerService() {
		return sessionHandlerService;
	}

	public void setSessionHandlerService(SessionHandlerService sessionHandlerService) {
		this.sessionHandlerService = sessionHandlerService;
	}

}
