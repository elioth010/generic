package com.elioth010.generic.security.service;

import com.elioth010.generic.security.eis.dto.SessionDTO;
import com.elioth010.generic.security.view.ws.exception.InvalidCredentialsException;
import com.elioth010.generic.security.view.ws.exception.WebServiceException;

public interface SessionHandlerService {

	public boolean validateCredentials(String username, String password) throws InvalidCredentialsException;

	/**
	 * Metodo que se encarga de validar el acceso a mediante el algoritmo
	 * criptografico MAC usando una clave compartida simetrica
	 */
	public void validateAccess(String key, String passw) throws WebServiceException;

	/**
	 * Metodo que se encarga de decriptar un password utilizando una clave
	 * compartida simetrica
	 */
	public String decryptPassword(String password) throws WebServiceException;

	/**
	 * Metodo que se encarga de crear una nueva sesion
	 */
	public String createSession(SessionDTO session) throws WebServiceException;

	/**
	 * Metodo que verfica si un token es valido y reinicia la actividad.
	 */
	public SessionDTO verifySession(String sessionToken, String ipAddress) throws WebServiceException;

	/**
	 * Metodo que devuelve la sesion sin reiniciar la actividad.
	 */
	public SessionDTO getSession(String sessionToken, String ipAddress) throws WebServiceException;

	/**
	 * Metodo que se encarga de finalizar una sesion
	 */
	public SessionDTO killSession(String sessionToken, String ipAddress) throws WebServiceException;

}
