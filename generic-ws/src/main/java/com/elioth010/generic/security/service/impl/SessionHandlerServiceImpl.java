package com.elioth010.generic.security.service.impl;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.xml.ws.WebServiceException;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.elioth010.generic.security.dao.SessionHandlerDao;
import com.elioth010.generic.security.eis.dto.SessionDTO;
import com.elioth010.generic.security.service.SessionHandlerService;
import com.elioth010.generic.security.view.ws.exception.InvalidCredentialsException;
import com.elioth010.generic.security.view.ws.exception.InvalidSessionException;

public class SessionHandlerServiceImpl implements SessionHandlerService {

	protected static final Logger logger = Logger.getLogger(SessionHandlerServiceImpl.class);

	private static final Random RANDOM = new Random(System.currentTimeMillis());
	private Base64 ENCODER = new Base64();
	// private BASE64Decoder DECODER = new BASE64Decoder();
	private Mac MAC;
	// private Cipher CIPHER;
	// public static final String CRYPTOGRAPHIC_HASH_FUNCTION = "HmacSHA256";
	// public static final String ALGORITHM_ENCRYPT_DECRYPT = "AES";
	// public static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
	// // private String sharedSymmetricKey;

	private List<String> anonymousChannels;
	Map<String, String> users;
	private SessionHandlerDao sessionHandlerDao;

	public SessionHandlerServiceImpl() {
		super();
	}

	public SessionHandlerServiceImpl(SessionHandlerDao sessionHandlerDao, String anonymousChannels, Map<String, String> users) {
		super();
		this.sessionHandlerDao = sessionHandlerDao;
		this.users = users;
		this.anonymousChannels = new ArrayList<String>(Arrays.asList(anonymousChannels.split(",")));
		logger.info("Canales Anonimos: " + this.anonymousChannels);
	}

	public void init() throws NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException {
		// MAC = Mac.getInstance(CRYPTOGRAPHIC_HASH_FUNCTION);
		// SecretKeySpec keySpec = new
		// SecretKeySpec(sharedSymmetricKey.getBytes(),
		// CRYPTOGRAPHIC_HASH_FUNCTION);
		// MAC.init(keySpec);
		//
		// CIPHER = Cipher.getInstance(TRANSFORMATION);
	}

	@Override
	public void validateAccess(String key, String passw) throws WebServiceException, InvalidCredentialsException {
		logger.debug("validateAccessNode:");
		try {
			byte[] result = MAC.doFinal(key.getBytes());
			String password = new String(ENCODER.encode(result));
			logger.debug(password);
			if (!password.equals(passw)) {
				throw new InvalidCredentialsException();
			}
			logger.debug("validateAccessNode  successful");
		} catch (Exception e) {
			e.printStackTrace();
			throw new InvalidCredentialsException();
		}
	}

	@Override
	public String decryptPassword(String password) throws WebServiceException {
		// try {
		// SecretKeySpec simetricKeyAES = new
		// SecretKeySpec(sharedSymmetricKey.getBytes(),
		// ALGORITHM_ENCRYPT_DECRYPT);
		// byte[] deco = DECODER.decodeBuffer(password);
		// CIPHER.init(Cipher.DECRYPT_MODE, simetricKeyAES, new
		// IvParameterSpec(new byte[sharedSymmetricKey.length()]));
		// byte[] original = CIPHER.doFinal(deco);
		// String decrypt = new String(original);
		// logger.debug("decryptPassword: " + decrypt);
		// return decrypt;
		// } catch (Exception e) {
		// e.printStackTrace();
		// throw new InvalidCredentialsException();
		// }
		return null;
	}

	@Override
	public String createSession(SessionDTO session) throws WebServiceException {
		byte[] array = new byte[15];
		session.setTimeMillis(System.currentTimeMillis());
		for (int i = array.length; --i >= 0;) {
			array[i] = (byte) RANDOM.nextInt(255);
		}
		String sessionToken = new String(ENCODER.encode(array));
		session.setToken(sessionToken);
		/** CERRAR SESIONES ABIERTAS POR USUARIO */
		List<SessionDTO> sessions = this.sessionHandlerDao.getSessionsByUsername(session.getUsername());
		if (sessions != null && sessions.size() > 0) {
			logger.debug("Se han encontrado sesiones activas para el usuario: " + session.getUsername());
			for (SessionDTO s : sessions) {
				this.sessionHandlerDao.deleteSession(s);
			}
		}
		this.sessionHandlerDao.createSession(session);
		logger.debug("createSession [" + session.getUsername() + ":" + sessionToken + "]");
		return sessionToken;
	}

	@Override
	public SessionDTO verifySession(String sessionToken, String ipAddress) throws WebServiceException, InvalidSessionException {
		logger.debug("verifySession [" + sessionToken + "]");
		if (this.anonymousChannels.contains(ipAddress)) {
			return new SessionDTO("anonymous", ipAddress);
		} else {
			SessionDTO session = this.sessionHandlerDao.getSession(sessionToken, ipAddress);
			if (session == null) {
				logger.error("No se ha encontrado la sesion [" + sessionToken + "]");
				throw new InvalidSessionException();
			} else {
				/** ACTUALIZACION DE TIEMPO POR ACTIVIDAD */
				session.setTimeMillis(System.currentTimeMillis());
				this.sessionHandlerDao.updateSession(session);
				return session;
			}
		}
	}

	@Override
	public SessionDTO getSession(String sessionToken, String ipAddress) throws WebServiceException, InvalidSessionException {
		logger.debug("getSession [" + sessionToken + "]");
		SessionDTO session = this.sessionHandlerDao.getSession(sessionToken, ipAddress);
		if (session == null) {
			logger.error("No se ha encontrado la sesion [" + sessionToken + "]");
			throw new InvalidSessionException();
		}
		return session;
	}

	@Override
	public SessionDTO killSession(String sessionToken, String ipAddress) throws WebServiceException, InvalidSessionException {
		logger.debug("killSession [" + sessionToken + ":" + ipAddress + "]");
		SessionDTO session = this.sessionHandlerDao.getSession(sessionToken, ipAddress);
		if (session == null) {
			throw new InvalidSessionException();
		}
		this.sessionHandlerDao.deleteSession(session);
		return session;
	}

	@Override
	public boolean validateCredentials(String username, String password) throws InvalidCredentialsException {
		String credentials;
		if ((credentials = this.users.get(username)) != null) {
			if (credentials.equals(password)) {
				return true;
			}
		}
		throw new InvalidCredentialsException();
	}
}
