package com.elioth010.generic.security.view.ws.exception;

public class WebServiceException extends Exception {

	private static final long serialVersionUID = 6604906314705942573L;

	private Integer codigo;
	private String descripcion;

	public WebServiceException(Integer codigo, String descripcion) {
		super(descripcion);
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}
}
