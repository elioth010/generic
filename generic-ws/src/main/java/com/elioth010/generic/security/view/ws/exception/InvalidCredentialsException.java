package com.elioth010.generic.security.view.ws.exception;


public class InvalidCredentialsException extends WebServiceException {

	private static final long serialVersionUID = -5254783262250159014L;

	private static final String MESSAGE = "InvalidCredentialsException";
	private static final Integer CODIGO = 993;

	public InvalidCredentialsException() {
		super(CODIGO, MESSAGE);
	}
}