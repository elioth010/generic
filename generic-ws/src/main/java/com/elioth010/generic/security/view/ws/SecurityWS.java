package com.elioth010.generic.security.view.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

import org.apache.log4j.Logger;

import com.elioth010.generic.security.view.ws.exception.InvalidCredentialsException;
import com.elioth010.generic.view.ws.AbstractWebServiceEndPoint;
import com.elioth010.generic.ws.eis.dto.HeaderResponseDTO;
import com.elioth010.generic.ws.eis.dto.LoginRequestDTO;
import com.elioth010.generic.ws.eis.dto.LoginResponseDTO;
import com.sun.xml.ws.developer.SchemaValidation;

@WebService(serviceName = "AuthenticationService", portName = "AuthenticationHTTPPort", targetNamespace = "http://ws.view.genericws.elioth.com/")
@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
@SchemaValidation
public class SecurityWS extends AbstractWebServiceEndPoint {

	protected static final Logger logger = Logger.getLogger(SecurityWS.class);

	@WebMethod(operationName = "Login")
	@WebResult(name = "LoginResponse")
	public LoginResponseDTO login(@WebParam(name = "LoginRequest") LoginRequestDTO request) {
		LoginResponseDTO response = new LoginResponseDTO();
		response.setHeader(new HeaderResponseDTO());
		response.setUsuarioWS(request.getUsuarioWS());
		response.setIp(super.getRemoteIp());
		try {
			/** VERIFICAR USUARIO */
			logger.info("Intentando Login: " + request.getUsuarioWS());
			super.validateCredentials(request.getUsuarioWS(), request.getPasswordWS());
			logger.info("Login Exitoso: " + request.getUsuarioWS());
		} catch (InvalidCredentialsException e) {
			logger.info("InvalidCredentialsException: " + request.getUsuarioWS());
			response.getHeader().setCodigoError(e.getCodigo());
			response.getHeader().setDescripcionError(e.getDescripcion());
		}

		return response;
	}
}
