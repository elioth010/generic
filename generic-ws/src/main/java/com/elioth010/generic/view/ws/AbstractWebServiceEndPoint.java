/**
 * 
 */
package com.elioth010.generic.view.ws;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.elioth010.generic.security.service.SessionHandlerService;
import com.elioth010.generic.security.view.ws.exception.InvalidCredentialsException;
import com.elioth010.generic.security.view.ws.exception.WebServiceException;
import com.elioth010.generic.ws.eis.dto.AbstractRequestDTO;

/**
 * @author elioth010
 *
 */
@Component
public abstract class AbstractWebServiceEndPoint {

	@Resource
	private WebServiceContext ctx;

	@Autowired
	private SessionHandlerService sessionHandlerService;

	public SessionHandlerService getSessionHandlerService() {
		return sessionHandlerService;
	}

	public void setSessionHandlerService(SessionHandlerService sessionHandlerService) {
		this.sessionHandlerService = sessionHandlerService;
	}

	public void verifySession(AbstractRequestDTO request) throws WebServiceException {
		this.sessionHandlerService.verifySession(request.getHeader().getSessionID(), this.getRemoteIp());
	}

	public String getRemoteIp() {
		Map<String, Object> msgContext = ctx.getMessageContext();
		HttpServletRequest req = (HttpServletRequest) msgContext
				.get(MessageContext.SERVLET_REQUEST);
		return req.getRemoteAddr();
	}

	public boolean validateCredentials(String username, String password) throws InvalidCredentialsException {
		return this.sessionHandlerService.validateCredentials(username,password);
	}

}
