package com.elioth010.generic.view.ws;

import java.sql.DriverManager;
import java.sql.Statement;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

import org.apache.log4j.Logger;

import com.elioth010.generic.security.view.ws.exception.WebServiceException;
import com.elioth010.generic.ws.eis.dto.HeaderResponseDTO;
import com.elioth010.generic.ws.eis.dto.LoginRequestDTO;
import com.elioth010.generic.ws.eis.dto.LoginResponseDTO;
import com.sun.xml.ws.developer.SchemaValidation;

/**
 * 
 * @author elioth010
 *
 */
@WebService(serviceName = "TestService", portName = "TestHTTPPort", targetNamespace = "http://ws.view.genericws.elioth.com/")
@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
@SchemaValidation
public class EjemploSpringWS extends AbstractWebServiceEndPoint {

	protected static final Logger logger = Logger.getLogger(EjemploSpringWS.class);

	@WebMethod(operationName = "Login")
	@WebResult(name = "LoginResponse")
	public LoginResponseDTO login(@WebParam(name = "LoginRequest") LoginRequestDTO request) {

		try {
			super.verifySession(request);
			logger.debug("test");
			return new LoginResponseDTO();
		} catch (WebServiceException e) {
			LoginResponseDTO response = new LoginResponseDTO();
			response.setHeader(new HeaderResponseDTO(e.getCodigo(), e.getDescripcion()));
			return response;
		}
	}

	@WebMethod(operationName = "Insert")
	@WebResult(name = "InsertResponse")
	public String insert(@WebParam(name = "usecod") int usecod, @WebParam(name = "usenom") String usenom, @WebParam(name = "agecod") int agecod, @WebParam(name = "jorcod") int jorcod,
			@WebParam(name = "ticcod") int ticcod, @WebParam(name = "niccod") int niccod, @WebParam(name = "useclv") String useclv, @WebParam(name = "useuac") int useuac,
			@WebParam(name = "useumc") int useumc, @WebParam(name = "useudc") int useudc, @WebParam(name = "useemp") String useemp, @WebParam(name = "useaco") int useaco,
			@WebParam(name = "usemco") int usemco, @WebParam(name = "usedco") int usedco, @WebParam(name = "usests") String usests, @WebParam(name = "depcod") int depcod,
			@WebParam(name = "usrmod") String usrmod, @WebParam(name = "eqcaje") int eqcaje) {

		String conexionBD = "jdbc:mysql://172.16.10.185:3308/JTELLDATV2";

		java.sql.Connection conexion = null;

		String message = "false";

		String query;

		try {

			Class.forName("com.mysql.jdbc.Driver");
			conexion = DriverManager.getConnection(conexionBD, "JTELLDAT", "jtelldat");
			Statement s = conexion.createStatement();

			query = "INSERT INTO JTELLDATV2.JTUSER VALUES ( " + usecod + " , '" + usenom + "' , " + agecod + " , " + jorcod + " , " + ticcod + " , " + niccod + " , '" + useclv + "' , " + useuac
					+ " , " + useumc + " , " + useudc + " , '" + useemp + "' , " + useaco + " , " + usemco + " , " + usedco + " , '" + usests + "' , " + depcod + " , '" + usrmod + "' , " + eqcaje
					+ ")";

			s.executeUpdate(query);
			message = "Ser insertaron valores a JTUSER";

		} catch (Exception e) {
			message = "e.getMessage()";
		}

		return message;
	}
}
