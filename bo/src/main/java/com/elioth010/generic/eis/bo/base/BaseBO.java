package com.elioth010.generic.eis.bo.base;

import java.io.Serializable;

public class BaseBO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6549111749074725204L;
	private Integer id;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
}
