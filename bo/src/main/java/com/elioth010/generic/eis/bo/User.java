package com.elioth010.generic.eis.bo;

import java.beans.Transient;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.OptimisticLocking;
import org.hibernate.annotations.Type;

import com.elioth010.generic.eis.bo.base.BaseBO;
import com.elioth010.generic.eis.persistence.audit.AuditLog;

@Entity
@Table(name = "USERS", schema = "GENERIC")
@OptimisticLocking(type = OptimisticLockType.VERSION)
public class User extends BaseBO implements AuditLog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7907195770553809351L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	@Version
	@Column(name = "VERSION")
	private Short version;
	@Column(name = "USERNAME", nullable = false)
	private String username;
	@Column(name = "PASSWORD", nullable = false)
	private String password;
	@Column(name = "EMAIL", nullable = false)
	private String email;
	@Column(name = "ENABLED")
	@Type(type = "yes_no")
	private Boolean enabled;
	@Column(name = "EXPIRED_ACCOUNT")
	@Type(type = "yes_no")
	private Boolean expiredAccount;
	@Column(name = "LOCKED_ACCOUNT")
	@Type(type = "yes_no")
	private Boolean lockedAccount;
	@Column(name = "EXPIRED_CREDENTIALS")
	@Type(type = "yes_no")
	private Boolean expiredCredentials;
	@Column(name = "LOGIN_ATTEMPTS")
	private Short loginAttempts;
	@Column(name = "LAST_LOGIN")
	private Date lastLogin;
	@Column(name = "LAST_PASSWORD_CHANGE")
	private Date lastPasswordChange;
	@Column(name = "CREATED")
	private Date created;
	@Column(name = "MODIFIED")
	private Date modified;
	@Column(name = "LAST_IP")
	private String lastIP;
	@Column(name = "DEFAULT_PROFILE_NAME")
	private String defaultProfileName;
	@Column(name = "FIRST_NAME")
	private String firstName;
	@Column(name = "SECOND_NAME")
	private String secondName;
	@Column(name = "FIRST_LAST_NAME")
	private String firstLastName;
	@Column(name = "SECOND_LAST_NAME")
	private String secondLastName;
	@Column(name = "LAST_PASSWORD_RESET")
	private Date lastPasswordReset;
	@Column(name = "PASSWORD_RESET")
	@Type(type = "yes_no")
	private Boolean passwordReset;
	@Column(name = "PASSWORD_EXPIRES")
	@Type(type = "yes_no")
	private Boolean passwordExpires;

	public User() {
		super();
	}

	public User(Integer id, Short version, String username, String password, String email, Boolean enabled, Boolean expiredAccount, Boolean lockedAccount, Boolean expiredCredentials,
			Short loginAttempts, Date lastLogin, Date lastPasswordChange, Date created, Date modified, String lastIP, String defaultProfileName, String firstName,
			String secondName, String firstLastName, String secondLastName, Date lastPasswordReset, Boolean passwordReset, Boolean passwordExpires) {
		super();
		this.id = id;
		this.version = version;
		this.username = username;
		this.password = password;
		this.email = email;
		this.enabled = enabled;
		this.expiredAccount = expiredAccount;
		this.lockedAccount = lockedAccount;
		this.expiredCredentials = expiredCredentials;
		this.loginAttempts = loginAttempts;
		this.lastLogin = lastLogin;
		this.lastPasswordChange = lastPasswordChange;
		this.created = created;
		this.modified = modified;
		this.lastIP = lastIP;
		this.defaultProfileName = defaultProfileName;
		this.firstName = firstName;
		this.secondName = secondName;
		this.firstLastName = firstLastName;
		this.secondLastName = secondLastName;
		this.lastPasswordReset = lastPasswordReset;
		this.passwordReset = passwordReset;
		this.passwordExpires = passwordExpires;
	}

	public User(String username, String password, String email, Boolean enabled, Boolean expiredAccount, Boolean lockedAccount, Boolean expiredCredentials, Short loginAttempts, Date lastLogin,
			Date lastPasswordChange, Date created, Date modified, String lastIP, String defaultProfileName, String firstName, String secondName, String firstLastName,
			String secondLastName, Date lastPasswordReset, Boolean passwordReset, Boolean passwordExpires) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.enabled = enabled;
		this.expiredAccount = expiredAccount;
		this.lockedAccount = lockedAccount;
		this.expiredCredentials = expiredCredentials;
		this.loginAttempts = loginAttempts;
		this.lastLogin = lastLogin;
		this.lastPasswordChange = lastPasswordChange;
		this.created = created;
		this.modified = modified;
		this.lastIP = lastIP;		
		this.defaultProfileName = defaultProfileName;
		this.firstName = firstName;
		this.secondName = secondName;
		this.firstLastName = firstLastName;
		this.secondLastName = secondLastName;
		this.lastPasswordReset = lastPasswordReset;
		this.passwordReset = passwordReset;
		this.passwordExpires = passwordExpires;
	}
	
	@Override
	@Transient
	public String getLogDeatil() {
		StringBuilder sb = new StringBuilder();
		sb.append(" User ID: ").append(this.getId())
		.append(" User username : ").append(this.getUsername())
		.append(" User email : ").append(this.getEmail())
		.append(" User password : ").append(this.getPassword())
		.append(" User email : ").append(this.getEmail())
		.append(" User lastIP : ").append(this.getLastIP())
		.append(" User firstName : ").append(this.getFirstName())
		.append(" User secondName : ").append(this.getSecondName())
		.append(" User firstLastName : ").append(this.getFirstLastName())
		.append(" User secondLastName : ").append(this.getSecondLastName())
		.append(" User lastPasswordReset : ").append(this.getLastPasswordReset())
		.append(" User lastPasswordChange: ").append(this.getLastPasswordChange());
		return sb.toString();
	}
	
	@Override
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Short getVersion() {
		return version;
	}

	public void setVersion(Short version) {
		this.version = version;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getExpiredAccount() {
		return expiredAccount;
	}

	public void setExpiredAccount(Boolean expiredAccount) {
		this.expiredAccount = expiredAccount;
	}

	public Boolean getLockedAccount() {
		return lockedAccount;
	}

	public void setLockedAccount(Boolean lockedAccount) {
		this.lockedAccount = lockedAccount;
	}

	public Boolean getExpiredCredentials() {
		return expiredCredentials;
	}

	public void setExpiredCredentials(Boolean expiredCredentials) {
		this.expiredCredentials = expiredCredentials;
	}

	public Short getLoginAttempts() {
		return loginAttempts;
	}

	public void setLoginAttempts(Short loginAttempts) {
		this.loginAttempts = loginAttempts;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public Date getLastPasswordChange() {
		return lastPasswordChange;
	}

	public void setLastPasswordChange(Date lastPasswordChange) {
		this.lastPasswordChange = lastPasswordChange;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public String getLastIP() {
		return lastIP;
	}

	public void setLastIP(String lastIP) {
		this.lastIP = lastIP;
	}

	public String getDefaultProfileName() {
		return defaultProfileName;
	}

	public void setDefaultProfileName(String defaultProfileName) {
		this.defaultProfileName = defaultProfileName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getFirstLastName() {
		return firstLastName;
	}

	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public Date getLastPasswordReset() {
		return lastPasswordReset;
	}

	public void setLastPasswordReset(Date lastPasswordReset) {
		this.lastPasswordReset = lastPasswordReset;
	}

	public Boolean getPasswordReset() {
		return passwordReset;
	}

	public void setPasswordReset(Boolean passwordReset) {
		this.passwordReset = passwordReset;
	}

	public Boolean getPasswordExpires() {
		return passwordExpires;
	}

	public void setPasswordExpires(Boolean passwordExpires) {
		this.passwordExpires = passwordExpires;
	}

}
