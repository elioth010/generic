/**
 * 
 */
package com.elioth010.generic.eis.persistence.audit;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.elioth010.generic.eis.bo.base.BaseBO;

/**
 * @author elioth010
 *
 */
@Table(name = "AUDIT_LOG", schema = "GENERIC")
public class AuditLogRecord extends BaseBO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4134683834486177272L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;
	@Column(name = "ACTION")
	private String action;
	@Column(name = "DETAIL")
	private String detail;
	@Column(name = "CREATE_DATE")
	private Date createdDate;
	@Column(name = "ENTITY_ID")
	private String entityId;
	@Column(name = "ENTITY_NAME")
	private String entityName;
	@Column(name = "CLIENT_IP")
	private String clientIP;
	@Column(name = "UPDATED_BY")
	private String updatedBy;

	public AuditLogRecord() {
		super();
	}

	public AuditLogRecord(Integer id, String action, String detail, Date createdDate, String entityId, String entityName, String clientIP, String updatedBy) {
		super();
		this.id = id;
		this.action = action;
		this.detail = detail;
		this.createdDate = createdDate;
		this.entityId = entityId;
		this.entityName = entityName;
		this.clientIP = clientIP;
		this.updatedBy = updatedBy;
	}
	
	

	public AuditLogRecord(String action, String detail, Date createdDate, String entityId, String entityName, String clientIP, String updatedBy) {
		super();
		this.action = action;
		this.detail = detail;
		this.createdDate = createdDate;
		this.entityId = entityId;
		this.entityName = entityName;
		this.clientIP = clientIP;
		this.updatedBy = updatedBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getClientIP() {
		return clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

}
